import {Component} from 'react';
import './style.css'

class Border extends Component{
    render(){
        return (
            <div className="Border">
                <h1>{this.props.title}</h1>
                <div style={this.props.style}>{this.props.children}</div>
            </div>
        )
    }
}

export default Border