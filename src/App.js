import './App.css';
import Border from './components/Border'

function App() {
  return (
    <>
      <Border title="Primeiro teste" style={{color:"#ff0000"}}>
        Primeiro componente
      </Border>
      <Border title="Segundo teste" style={{"font-size":"48px"}}>
        Segundo componente
      </Border>
    </>
  );
}

export default App;
